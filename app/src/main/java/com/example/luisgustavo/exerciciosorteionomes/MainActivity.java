package com.example.luisgustavo.exerciciosorteionomes;

/* Importações feitas. */

import android.content.Context;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;
import android.os.Vibrator;

import java.util.ArrayList;
import java.util.Random;

public class MainActivity extends AppCompatActivity {

                       /* Declaração das variáveis utilizadas. */
    TextView           campoDeTextoMasc;                       /* Variável de texto que aparece os nomes masculinos. */
    TextView           campoDeTextoFem;                       /* Variável de texto que aparece os nomes femininos. */
    ArrayList <String> nomesMasculinos;                      /* Array de string onde ficam os nomes masculinos. */
    ArrayList <String> nomesFemininos;                      /* Array de string onde ficam os nomes femininos. */
    Random             random;                             /* Variável random que serve para gerar os nomes aleatoriamente.  */
    int                numAleatorio;                      /* Recebe a posição do nome a ser exibido. */
    int                numComparativo;                   /* Recebe a posição e verifica se não foi a última a ser exibida.*/
    boolean            limpo = true;                    /* Variável que permite verificar se a caixa de texto está vazia ou não. */
    Button             botao_masculino;                /* Variável do botão masculino. */
    Button             botao_feminino;                /* Variável do botão masculino. */
    Button             botao_limpa;                  /* Variável do botão apagar. */
    Animation          animFadeinOne;               /* Variável de animação do botão masculino. */
    Animation          animFadeinTwo;              /* Variável de animação do botão masculino. */
    Spinner            spinner;
    Spinner            spinnerfeminino;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

                           /* Inicialização das variáveis */
        campoDeTextoMasc = (TextView) findViewById(R.id.exibi_nomes);                                         /* Inicialização do campo de texto que exibirá os nomes masculinos. */
        campoDeTextoFem  = (TextView) findViewById(R.id.exibi_nomes_fem);                                    /* Inicialização do campo de texto que exibirá os nomes femininos. */
        nomesMasculinos  = new ArrayList<>();                                                               /* Nomes masculinos recebe um array list. */
        nomesFemininos   = new ArrayList<>();                                                              /* Nomes femininos recebe um array list. */
        botao_masculino  = (Button) findViewById(R.id.botao_masculino);                                   /* Inicialização do botao masculino. */
        botao_feminino   = (Button) findViewById(R.id.botao_feminino);                                   /* Inicialização do botao feminino. */
        botao_limpa      = (Button) findViewById(R.id.botao_feminino);                                  /* Inicialização do botao apagar. */
        animFadeinOne    = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.fade_in_one);  /* Inicialização da animação do botao masculino. */
        animFadeinTwo    = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.fade_in_two); /* Inicialização da animação do botao feminino. */
        spinner = (Spinner) findViewById(R.id.spinner);
        spinnerfeminino = (Spinner) findViewById(R.id.spinnerfeminino);
        //Identifica o Spinner no layout
        //Cria um ArrayAdapter usando um padrão de layout da classe R do android, passando o ArrayList nomes
        ArrayAdapter<String> arrayAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_dropdown_item, nomesMasculinos);
        ArrayAdapter<String> spinnerArrayAdapter = arrayAdapter;
        spinnerArrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_item);
        spinner.setAdapter(spinnerArrayAdapter);

        ArrayAdapter<String> arrayAux = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_dropdown_item, nomesFemininos);
        ArrayAdapter<String> auxArrayAdapter = arrayAux;
        auxArrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_item);
        spinnerfeminino.setAdapter(auxArrayAdapter);


        /* Adiciona os seguintes nomes ao array list nomesMasculinos. */
        nomesMasculinos.add("Luis");
        nomesMasculinos.add("Andrei");
        nomesMasculinos.add("Lucas");
        nomesMasculinos.add("Pedro");
        nomesMasculinos.add("Guilherme");
        nomesMasculinos.add("Gustavo");
        nomesMasculinos.add("Rodrigo");
        nomesMasculinos.add("Juan");
        nomesMasculinos.add("Thiago");
        nomesMasculinos.add("Geison");

        /* Adiciona os seguintes nomes ao array list nomesFemininos. */
        nomesFemininos.add("Nina");
        nomesFemininos.add("Leticia");
        nomesFemininos.add("Taine");
        nomesFemininos.add("Juliana");
        nomesFemininos.add("Carol");
        nomesFemininos.add("Tayna");
        nomesFemininos.add("Luana");
        nomesFemininos.add("Kim");
        nomesFemininos.add("Debora");
        nomesFemininos.add("Ana");
    }

                                      /* Criação dos métodos para cada botão. */


    public void mostraMasculinos(View v) {                                             /* Botão que mostra os nomes masculinos. */
        botao_masculino.startAnimation(animFadeinOne);                                /* Inicia a animação do botão. */
        gerarNome();                                                                 /* Gera número da posição para exibir nome*/

        if (numComparativo == numAleatorio && limpo == true) {                     /* Caso números sejam iguais e caixa de texto vazia. */
            numAleatorio--;                                                       /* Decrementa numAleatório */
            if (numAleatorio == -1) {                                            /* Caso numAleatório, quando decrementado, fique negativo. */
                numAleatorio = numAleatorio + 2;                                /* Adiciona mais 2 a numAleatório e ele volta a ficar positivo.*/
                campoDeTextoMasc.setText(nomesMasculinos.get(numAleatorio));   /* Vai ser setado o número da posição e será exibido o nome que estiver nessa posição do array masculino. */
            }
            campoDeTextoMasc.setText(nomesMasculinos.get(numAleatorio));     /* Vai ser setado o número da posição e será exibido o nome que estiver nessa posição do array masculino. */
        }

        if (limpo == true && numComparativo != numAleatorio) {            /* Caso números sejam diferentes e a caixa de texto vazia. */
            campoDeTextoMasc.setText(nomesMasculinos.get(numAleatorio)); /* Vai ser setado o número da posição e será exibido o nome que estiver nessa posição do array masculino. */
        }

        if (limpo == false) {                                       /* Caso caixa de texto com conteúdo. */
            vibrar();                                              /* Vibra e aparece mensagem solicitando para limpar a tela. */
        }

        estadoComparacao();                                     /* Estado da caixa de texto e comparação com nome anterior. */
    }

    public void mostraFemininos(View v) {                                            /* Botão que mostra os nomes femininos. */
        botao_feminino.startAnimation(animFadeinTwo);                               /* Inicia a animação do botão. */
        gerarNome();                                                               /* Gera número da posição para exibir nome*/

        if (limpo == true && numComparativo == numAleatorio) {                   /* Caso números iguais e caixa de texto vazia. */
            numAleatorio--;                                                     /* Decrementa numAleatorio */
            if (numAleatorio == -1) {                                          /* Caso numAleatorio, quando decrementado, fique negativo. */
                numAleatorio = numAleatorio + 2;                              /* Adiciona mais 2 a numAleatório e ele volta a ficar positivo.*/
                campoDeTextoFem.setText(nomesFemininos.get(numAleatorio));   /* Vai ser setado o número da posição e será exibido o nome que estiver nessa posição do array feminino. */
            }
            campoDeTextoFem.setText(nomesFemininos.get(numAleatorio));     /* Vai ser setado o número da posição e será exibido o nome que estiver nessa posição do array feminino. */
        }

        if (limpo == true && numComparativo != numAleatorio) {          /*  Caso caixa de texto vazia e números diferentes. */
            campoDeTextoFem.setText(nomesFemininos.get(numAleatorio)); /* Vai ser setado o número da posição e será exibido o nome que estiver nessa posição do array masculino. */
        }

        if (limpo == false) {                                      /* Caso caixa de tempo com conteúdo. */
            vibrar();                                             /* Vibra e aparece mensagem solicitando para limpar a tela. */
        }

        estadoComparacao();                                   /* Estado da caixa de texto e comparação com nome anterior. */
    }

    public void limpaTexto(View v) {   /* Botão que limpa a caixa de texto. */
        campoDeTextoMasc.setText(""); /* Vai setar para um campo vazio. */
        campoDeTextoFem.setText(""); /* Vai setar para um campo vazio. */
        limpo = true;               /* Deixa campo de texto vazio. */
    }

    public void vibrar() {
        Toast.makeText(MainActivity.this, "Apague o nome já existente!", Toast.LENGTH_SHORT).show(); /* Mensagem solicitando apagar nome. */
        Vibrator vibra = (Vibrator) getSystemService(Context.VIBRATOR_SERVICE);
        vibra.vibrate(500);                                                                         /* Vibra por 500ms. */
    }

    public void estadoComparacao() {
        limpo = false;                  /* Após setado, a caixa de texto fica com conteúdo, portanto necessita ser limpada.*/
        numComparativo = numAleatorio; /* numComparativo recebe o valor que está sendo exibido para comparar e evitar repetições */
    }

    public void gerarNome() {
        random = new Random();                                   /* Gera um número aleatório. */
        numAleatorio = random.nextInt(nomesFemininos.size());   /* Dentro de 10 posições, um número será passado para a variável numAleatorio. */
        numAleatorio = random.nextInt(nomesMasculinos.size()); /* Dentro de 10 posições, um número será passado para a variável numAleatorio. */
    }
}
